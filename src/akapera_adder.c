#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>

typedef struct stereo_sample
{
    int16_t left;
    int16_t right;
} stereo_sample;

typedef struct large_sample
{
    int32_t left;
    int32_t right;
} large_sample;

int16_t truncate(int32_t value);

int main(int argc, char **argv)
{

    // TODO: fixme
    argc--;
    FILE **filearr = malloc((argc-1) * sizeof(void *));

    if (argc < 2) exit(1);

    for (int i = 1; i <= argc; i++)
    {
        printf("Opening file: %s\n", argv[i]);
        fflush(stdout);
        filearr[i-1] = fopen(argv[i],"r");
    }

    struct stat fileStat;
    if (stat(argv[1], &fileStat) < 0) return 1;

    size_t main_size = fileStat.st_size / sizeof(stereo_sample);

    // Alloc main buf
    const size_t main_buf_size = main_size * sizeof(large_sample);
    large_sample *main_buf = malloc(main_buf_size);
    memset(main_buf, 0, main_buf_size);

    // Alloc buf for current file
    const size_t curr_buf_size = main_size * sizeof(stereo_sample);
    stereo_sample *curr_buf = malloc(curr_buf_size);

    for (int i = 1; i <= argc; i++)
    {

        // Get data
        memset(curr_buf, 0, curr_buf_size);
        fread(curr_buf, curr_buf_size, 1, filearr[i-1]);
        
        // Add data in to combination buffer
        for(int j = 0; j < main_size; j++)
        {
            main_buf[j].left  += curr_buf[j].left;
            main_buf[j].right += curr_buf[j].right;
        }

        fclose(filearr[i-1]); 

    }

    // Alloc output buf
    const size_t output_buf_size = main_size * sizeof(stereo_sample);
    stereo_sample *output_buf = malloc(output_buf_size);
    memset(output_buf, 0, output_buf_size);
    
    for(int i = 0; i < main_size; i++)
    {
        output_buf[i].left  = truncate(main_buf[i].left / argc);
        output_buf[i].right = truncate(main_buf[i].right / argc);
    }

    FILE *output = fopen("out.raw","w");
    fwrite(output_buf, output_buf_size, 1, output);

}

// Turn a int32_t to an int16_t
int16_t truncate(int32_t value)
{
    int result;
    if (value > (int32_t)INT16_MAX)
    {
        result = INT16_MAX;
    }
    else if (value < (int32_t)INT16_MIN)
    {
        result = INT16_MIN;
    }
    else
    {
        result = (int16_t)value;
    }

    return result;
}
