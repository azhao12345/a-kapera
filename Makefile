CC = gcc
CFLAGS = -g -O3 -Wall -std=c11
SRCDIR = src
BINDIR = bin

BINS = akapera akapera_aligner akapera_adder

all: $(addprefix $(BINDIR)/,$(BINS))

$(BINDIR)/akapera: $(SRCDIR)/akapera.c
	$(CC) $(CFLAGS) $(SRCDIR)/akapera.c -o $(BINDIR)/akapera

$(BINDIR)/akapera_aligner: $(SRCDIR)/akapera_aligner.c
	$(CC) $(CFLAGS) $(SRCDIR)/akapera_aligner.c -o $(BINDIR)/akapera_aligner

$(BINDIR)/akapera_adder: $(SRCDIR)/akapera_adder.c
	$(CC) $(CFLAGS) $(SRCDIR)/akapera_adder.c -o $(BINDIR)/akapera_adder

.PHONY : clean test align

test: akapera
	$(BINDIR)/akapera

align: akapera_aligner
	$(BINDIR)/akapera_aligner

clean:
	rm -f $(BINDIR)/akapera $(BINDIR)/akapera_aligner $(BINDIR)/akapera_adder

