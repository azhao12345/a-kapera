A-kapera
========

Create acapella tracks given off vocal version


SETUP
=====
1) Put your .flac input files in ```<rootdir>/input/```
2) Name your raw instrumental file ```<rootdir>/inst.raw```
3) Run ```./process.sh``` from the root directory. Output files will appear in ```./output/```
