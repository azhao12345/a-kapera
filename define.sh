#!/bin/bash

# Definitions
akapera_ver=akapera
#akapera_ver=akapera_aligner

workdir=$(pwd)
bindir=$workdir/bin
indir=$workdir/input
outdir=$workdir/output

logfile="process.log"
infile="vocal.raw"
outfile="out.raw"
encodeout="out.flac"
files="$infile $outfile $encodeout"
