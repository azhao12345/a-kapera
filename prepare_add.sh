#!/bin/bash

source define.sh
rename 's/\ /_/g' $indir/*.flac

mkdir -p rawdir

for i in $(ls input/*.flac)
do
    $bindir/decode.sh $i rawdir/$(basename $i.raw)
done

$bindir/akapera_adder $(ls rawdir/*.raw)

for i in $(ls rawdir/*.raw)
do
    $bindir/encode.sh $i rawdir/$(basename $i.raw)
done
