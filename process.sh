#!/bin/bash

# Process all .flac files in $indir
# Should be run with ./process in the A-kapera dir

source define.sh

cd $workdir
mkdir -p $indir $outdir

# Check for any input files
ls $indir/*.flac > /dev/null
if [ $? != 0 ]; then
  echo "Error: no input files"
  exit 1
fi

# Check inst.raw exists
if ! [ -f $workdir/inst.raw ]; then
  echo "Error: need inst.raw to run"
  exit 1
fi

rm -f $files
rm -f $logfile

rename 's/\ /_/g' $indir/*.flac

for i in $(ls $indir/*.flac)
do
  echo ">> Processing $(basename $i)"
  $bindir/decode.sh $i $infile &>> $logfile
  $bindir/$akapera_ver &>> $logfile
  if [ $? != 0 ]; then
    echo "Error: $akapera_ver failure"
    exit 1
  fi
  $bindir/encode.sh &>> $logfile
  mv $workdir/$encodeout $outdir/$(basename $i)
  mv $i $i.done
  rm -f $files
done
